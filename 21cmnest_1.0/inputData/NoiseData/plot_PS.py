import numpy
#import 
from decimal import *
import string
import matplotlib.pyplot as plt

redshift = [10.0]

fig1, ax1 = plt.subplots()
fig2, ax2 = plt.subplots()

SIXPLACES = Decimal(10) ** -1
separator = "_"

ax1.set_title('The 21cm Power Spectrum as a function of $R_{mfp}$ for [$\zeta = 20.$, $log_{10}[T_{vir}]=4.477$] ')
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_xlabel("$k[Mpc^{-1}]$")
ax1.set_ylabel("$PS$")

ax2.set_title('The 21cm Power Spectrum as a function of $R_{mfp}$ for [$\zeta = 20.$, $log_{10}[T_{vir}=4.477]$] ')
ax2.set_xscale('log')
ax2.set_yscale('log')
ax2.set_xlabel("$k[Mpc^{-1}]$")
ax2.set_ylabel("$PS$")


params = [20.,15.,30000,0.] 

seq = []
seq.append('Redshift')

for i in range(len(params)):    
	seq.append("%s"%(Decimal(repr(params[i])).quantize(SIXPLACES)))

	Individual_ID = redshift[0] 

seq[0] = ("z%s"%(Individual_ID))

StringArgument = string.join(seq,separator)

#DATA = numpy.loadtxt("MockObs_PS_250Mpc_%s.txt"%(StringArgument))
DATA = numpy.loadtxt("TotalError_HERA331_PS_250Mpc_%s_1000hr.txt"%(StringArgument))

	
ax1.plot(DATA[:,0], DATA[:,1])
fig1.savefig('HERA-331_250Mpc_%s_1000hr.png'%(redshift))

#ax2.plot(DATA[:,0], DATA[:,1])	

#fig1.savefig('Rmfps_z%s_PS_1.png'%(redshift))
#fig2.savefig('Rmfps_z%s_PS_2.png'%(redshift))










