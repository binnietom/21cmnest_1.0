import numpy
import matplotlib.pyplot as plt
from decimal import *
import string
import os

from matplotlib import rcParams
rcParams["font.size"] = 14


Redshifts = [8.]
models = ['FZH', 'F_MHR']#,'FZHinv','F_MHR', 'F_MHRinv', '1p_MHR', '1p_MHRinv']#'FZHinv'],'FZHinv', '1p_MHR', '1p_MHRinv']



NF_fzh = [12.,1.,4.] 
NF_F_MHR = [10., 3.5, 4.0]
NF_F_MHRinv = [10., 3.5, 4.0]  
# these give 0.5 NF (1sf)


def Driver(redshift, model, params):

 	SIXPLACES = Decimal(10) ** -6
	separator = " "
	seq = []
	seq.append('Redshift')
	for i in range(len(params)):    
		seq.append("%s"%(Decimal(repr(params[i])).quantize(SIXPLACES)))
	for i in range(len(redshift)):
	# Run 21CMMC (the 21cmFAST code) to perform the reionisation simulation and recover the neutral fraction and 21cm PS

		Individual_ID = redshift[i] #here I use a redshift id thread rather than a random one (like in the actual streamlined driver)
		seq[0] = ("z%s"%(Individual_ID))


		StringArgument = string.join(seq,separator)
		
		if (model == 'FZH'):
			command = "./21cmFAST/drive_21cmMC_streamlined %g %s 1"%(redshift[i],StringArgument) #run the driver from this file's location
		if (model == 'MHR' or model == 'F_MHR' or model == '1p_MHR'):	
			command = "./21cmFAST/drive_MHR_streamlined %g %s 1"%(redshift[i],StringArgument) 
		if (model == 'FZHinv'):
			command = "./21cmFAST/drive_FZHinv_streamlined %g %s 1"%(redshift[i],StringArgument) 	
		if (model == 'MHRinv' or model == 'F_MHRinv' or model == '1p_MHRinv'):
			command = "./21cmFAST/drive_MHRinv_streamlined %g %s 1"%(redshift[i],StringArgument)
			
		#command = "./21cmFAST/ComputingTau_e	
		print command
		os.system(command)


###need same neutral fractions

"""
ParamsF_ska = [0.203025231508187787E+02, 0.195698785005491196E+02, 0.443977183902213834E+01] #ML 3pf1
ParamsFi_ska = [0.704048213455259031E+02, 0.130444081315545718E+00, 0.530494747177384696E+01] 
ParamsF_MHR_ska = [0.181502224547583836E+03, 0.351774095227360295E+01, 0.528437217409049520E+01]
ParamsF_MHRinv_ska =  [0.171644145096983209E+03, 0.316600047230625226E+01, 0.526373711850877157E+01]  
ParamsM2_ska = [0.330372481250454371E+02, 0.472140146983694642E+01]
ParamsMi2_ska = [ 0.246031299953343478E+03, 0.557735846466022966E+01]
"""

Params1pM = [0.5]
Params1pMi = [0.5]

#for i in range(len(models)):!!
#	Driver(Redshifts, models[i])!!

#'FZH','FZHinv','MHR','MHRinv','F_MHR',

pixels = 128

#pickslices in looop

for p in range(len(Redshifts)): 
	for k in range(len(models)):
		
		if(models[k] == 'FZH'):
			params = [0.203025231508187787E+02, 0.195698785005491196E+02, 0.443977183902213834E+01]
		if(models[k] =='FZHinv'):
			#params =  [0.704048213455259031E+02, 0.130444081315545718E+00, 0.530494747177384696E+01] 
			params = NF_fzh
		if(models[k] =='MHR'):
			params = [0.330372481250454371E+02, 0.472140146983694642E+01]
		if(models[k] =='MHRinv'):
			params = [ 0.246031299953343478E+03, 0.557735846466022966E+01]
		if(models[k] =='F_MHR'):
			#params = [0.181502224547583836E+03, 0.351774095227360295E+01, 0.528437217409049520E+01]
			params = NF_F_MHR

		if(models[k] =='F_MHRinv'):
			#params = [0.171644145096983209E+03, 0.316600047230625226E+01, 0.526373711850877157E+01] 
			params = NF_F_MHRinv

		if(models[k] =='1p_MHR'):
			params = [0.5]
			models[k] = 'MHR'
		if(models[k] =='1p_MHRinv'):
			params = [0.5]
			models[k] = 'MHRinv'

		if (p==0): Driver(Redshifts, models[k], params)

		boxslice = numpy.loadtxt('21cmFAST/delTps_boxslice_'+models[k]+'_8.txt')  
		#boxslice = numpy.loadtxt('../DATA/brightnessT_MAPparam_slices/NF=0.5/delTps_boxslice_'+models[k]+'_8.txt')  
		# xyz are integers [0,127] pixels corresponding to 250 Mpc cubes

		sliced = numpy.zeros((pixels,pixels))

		for i in range(1,pixels):
			for j in range(pixels):
				sliced[j,i] = boxslice[(pixels*(i)+j)]

		fig, ax = plt.subplots()
		c = ax.pcolor(sliced)

		fig.colorbar(c, ax=ax)
		
		
		pS = '_'
		print
		for d in range(len(params)):    
			if (d>0): pS = pS + '_'
			pS = pS + str(Decimal(repr(params[d])).quantize(Decimal(10) ** -6))

		#print pS
	
		NFStringArgument = 'z'+str(Redshifts[p])+pS
		print NFStringArgument
		NF = numpy.loadtxt('NeutralFraction_%s.txt'%(NFStringArgument), usecols=(0,))		
		
		NF = numpy.round_(NF, decimals = 1)

		ax.set_title(models[k]+' $\delta T_b$ [mK] Box Slice (z='+str(int(Redshifts[p]))+'; $x_{HI}=$'+str(NF)+')')
		ax.set_xlabel('pixels[250/128 Mpc]')
		ax.set_ylabel('pixels[250/128 Mpc]')
#ax.set_colorlabel('Brightness Temperature [mK]')
		plt.savefig(models[k]+'_boxslice22_'+str(int(Redshifts[p]))+'.pdf')



"""
###DENSITY FIELD PLOTTING
Driver(Redshifts, models[0])

boxslice = numpy.loadtxt('21cmFAST/delx_boxslice_8.txt')   # xyz are integers [0,127] pixels corresponding to 250 Mpc cubes

print len(boxslice)


sliced = numpy.zeros((pixels,pixels))

for i in range(1,pixels):
	for j in range(pixels):
		#if (boxslice[(pixels*(i)+j)] == 0.0): print i, j
		sliced[i,j] = boxslice[(pixels*(j)+i)]

fig, ax = plt.subplots()
c = ax.pcolor(sliced)

fig.colorbar(c, ax=ax)


ax.set_title('Density Field Slice z=8 ')
ax.set_xlabel('pixels[250/128 Mpc]')
ax.set_ylabel('pixels[250/128 Mpc]')

plt.savefig('delx_boxslice_z8.png')
"""

