import corner
import numpy 
from matplotlib import rcParams
rcParams["font.size"] = 14.

infile = numpy.loadtxt('ReionModel_21CMNest_HERA331_multizpost_equal_weights.dat')

fig = corner.corner(infile,   
		    bins = 20, 
		    range = [(5.,250.),(5.,20.),(4.,5.3), (-50.,0.)],   #set for 3pFZH
		    labels=['$\zeta$', '$R_{mfp}$' ,'$Log_{10}(T_{vir})$','$Ln(Liklihood)$'],
		    truths=[20., 15., 4.477, None],  #f1
		    color = 'darkmagenta',
		    smooth=(1.0),	
		    smooth1d=(0.0),	
		    plot_contours=True,
		   )

fig.savefig('Params+loglike.png')

infile2 = numpy.loadtxt('NF_21cmFAST.txt')

fig2 = corner.corner(infile2,   
		    bins = 20, 
		    range = [1.,1.,1.], 
		    labels=['$z=8$', '$z=9$' ,'$z=10$'],
		    color = 'darkmagenta',
		    smooth=(1.0),	
		    smooth1d=(0.0),	
		    plot_contours=True,
		   )

fig2.savefig('Neutral_fractions.png')
