import random
import os
import numpy as np
import pickle
np.seterr(invalid='ignore', divide='ignore')
from decimal import *
from scipy import interpolate
from scipy.interpolate import InterpolatedUnivariateSpline
import string
import matplotlib.pyplot as plt

from matplotlib import rcParams
rcParams["font.size"] = 14

Drive = False  #run the simulation
filename = 'drift'

Redshift = [9]#[8,9,10]
redshifts = [9.0]#['8.0','9.0','10.0']

nametag = str(filename)

#notation: M=3pMHR=F_MHR   ,  M2=2pMHR  

#Params = [19.289497,16.112542,4.353285, -0.0000001]
#Params = [15.,15.,4.699, -0.2]

#Params = [20.,15,4.477, 1e-5] #specify fiducials here
#Params = [15.,15.,4.699, 0.4]

#Params = [60.,15,4.48]   # original 21CMMC paper values

Telescope = 'LOFAR-DRIFT'#'HERA331' #'SKA512' #HERA331 or LOFAR48 , SKA866
Obs_time = '1080hrs'#'10800hrs'#'4320hrs'#'21600hrs'#'1080hrs'#

""" These are 10hr track for LOFAR -- need 1 hr track for a sensible observation (horizon effects)
ParamsF_lofar = [0.253813174366950989E+02,0.195718157291412354E+02 ,  0.454980303645133954E+01] #ML 3pf1
ParamsFi_lofar = [ 0.100497890668980272E+02, 0.173906922187183994E+02, 0.400227432556050200E+01] 
ParamsM_lofar = [0.200096277668319345E+03,  0.537795650561055272E+01, 0.558221353078508997E+01]
ParamsMi_lofar =  [0.276729679339647419E+03, 0.877691989794994498E+01, 0.535042834527699007E+01]
ParamsM2_lofar = [0.281607415969459502E+02, 0.476465201858591492E+01]
ParamsMi2_lofar = [0.254404377825693956E+03, 0.565675568955511743E+01]
"""

ParamsF_lofar = [0.126469398140907288E+03, 0.187875905632972717E+02, 0.519955282365964155E+01] #MAP 3pf1
ParamsFi_lofar = [0.136697464891223035E+03, 0.160344543647857135E+00, 0.539743702623724175E+01] 
#ParamsM_lofar = [0.489098957835749161E+03, 0.260617649933170137E+01, 0.576967958605572573E+01]
#ParamsMi_lofar =  [0.632058278840272237E+03, 0.304444845919517793E+01, 0.583745467372652982E+01] (before the MHR driver labelling cockup)
ParamsMi_lofar = [0.489098957835749161E+03, 0.260617649933170137E+01, 0.576967958605572573E+01]
ParamsM_lofar =  [0.632058278840272237E+03, 0.304444845919517793E+01, 0.583745467372652982E+01]

#ParamsM2_lofar = [0.328495519459247589E+03, 0.566335356235504150E+01]
#ParamsMi2_lofar = [0.593543734645555446E+03, 0.581520916781285457E+01]
ParamsMi2_lofar = [0.328495519459247589E+03, 0.566335356235504150E+01]
ParamsM2_lofar = [0.593543734645555446E+03, 0.581520916781285457E+01]

"""866
ParamsF_ska = [0.214924744789696049E+02,0.196608499746075474E+02,0.448566506577794755E+01] #ML 3pf1
ParamsFi_ska = [0.932298217035043280E+02 ,0.539780730390593977E+01,0.400112098359844381E+01] 
ParamsM_ska = [0.711685466817104043E+03, 0.500725100897348163E+01,0.569797407870650385E+01]
ParamsMi_ska =  [0.258739412715848744E+03, 0.500089584246994967E+01,0.540748603053443766E+01]  
ParamsMi2_ska = [0.493277132736259432E+03, 0.572781720752481682E+01]
ParamsM2_ska = [0.445864347517057809E+02, 0.486967297112395947E+01]
"""

"""512""" """
ParamsF_ska = [0.203025231508187787E+02, 0.195698785005491196E+02, 0.443977183902213834E+01] #ML 3pf1
ParamsFi_ska = [0.704048213455259031E+02, 0.130444081315545718E+00, 0.530494747177384696E+01] 
#ParamsM_ska = [0.181502224547583836E+03, 0.351774095227360295E+01, 0.528437217409049520E+01]
#ParamsMi_ska =  [0.171644145096983209E+03, 0.316600047230625226E+01, 0.526373711850877157E+01]  (before the MHR driver labelling cockup)
ParamsMi_ska = [0.181502224547583836E+03, 0.351774095227360295E+01, 0.528437217409049520E+01]
ParamsM_ska =  [0.171644145096983209E+03, 0.316600047230625226E+01, 0.526373711850877157E+01]
#ParamsM2_ska = [0.330372481250454371E+02, 0.472140146983694642E+01]
#ParamsMi2_ska = [ 0.246031299953343478E+03, 0.557735846466022966E+01]
ParamsMi2_ska = [0.330372481250454371E+02, 0.472140146983694642E+01]
ParamsM2_ska = [ 0.246031299953343478E+03, 0.557735846466022966E+01]
#SKA512 double check params
"""
ParamsF_ska = [20.3, 19.6, 4.44] 
ParamsFi_ska = [70.4, 0.13, 5.3] 

ParamsMi_ska = [182., 3.5, 5.3]
ParamsM_ska =  [171., 3.2, 5.3]

ParamsMi2_ska = [43., 4.7]
ParamsM2_ska = [320., 5.6]

#defaults are HERA331

#Params =  [0.195116761042458720E+02, 0.155622409208508330E+02, 0.439599548122683714E+01]   # FHZ 3pf1 mean fit
#Params = [0.143799543821033922E+02, 0.119093248261802476E+02, 0.352128489811591594E+01]   # invFHZ mean points
#ParamsMi = [ 0.999481215222705487E+03,0.169134072678467433E+02,0.592200270514035054E+01]	#MHRinv

#ParamsFi = [0.140697405472630450E+02, 0.118542309654427385E+02, 0.350007804495945996E+01] # invFHZ maxlike points
#ParamsF = [0.188076253087948579E+02, 0.143651248969884406E+02, 0.437900486269548761E+01] #FHZ maxlike points

#ParamsF=[20.,15.,4.447]

#unscaled MHR (pixel by pixel) best fit points.
#ParamsM = [0.740364528511637729E+02, 0.132996907680971237E+02, 0.513901877958315367E+01]  #MHR
""" THESE HAVE K-SPACE TOP HAT FILTER 
ParamsM =  [0.133784640123746669E+03, 0.711026050427831890E+01 , 0.532295290679231137E+01]
ParamsMi = [0.116764133815559930E+03,  0.119386017975228533E+02, 0.542399160459342777E+01]
"""



#ParamsM2 = [0.710091236966496240E+02, 0.512219338973239946E+01]
#ParamsMi2 = [0.177682436089534440E+04, 0.601939155417568372E+01]

#M2 = [0.198979312072228929E+02, 0.427956987038488990E+01]

####for F3 NF-based plot
"""
#ParamsM2 = [30., 4.5]fiducial3
ParamsM2 = [0.198979312072228929E+02 , 0.427956987038488990E+01]
ParamsM1 = [0.41399]
ParamsM1z = [ 0.366641467261741016E+00, 0.487758214564270765E+00,0.659588221184393642E+00 ]
"""

#HERA331 MAP points (in paper):
"""
ParamsF = [18.8, 14.4, 4.38] 
ParamsFi =[0.143813115781190754E+04,0.100310177679685730E+01,0.613263445116078110E+01]		#ParamsFi = [139, 0.55, 5.64] 


ParamsMi2 = [71., 5.12]
ParamsM2 =  [1960.,6.02]





ParamsMi = [203.,4.35, 5.32]	
ParamsM = [432., 4.65, 5.56]  #  1

ParamsMi = [332.,4.7, 5.46]	
ParamsM = [461., 4.6, 5.58]			# 2 - doublecheck 

ParamsMi = [295.,5., 5.45]	
ParamsM = [440., 5., 5.57]			# 3 - doublecheck 

ParamsM = [498.,5.2, 5.6]	
ParamsMi = [487., 5.3, 5.59]			# 4 - doublecheck 
"""

ParamsF = [18.8, 14.4, 4.38] 	
ParamsFi = [ 139., 0.55, 5.64]
ParamsMi2 = [71., 5.12]
ParamsM2 =  [1960.,6.02]
ParamsMi = [203.,4.35, 5.3]
ParamsM = [432., 4.65, 5.56]



#ps+tscope

Lstyle = ['-','-','--', '-', '--', '-', '--']
colour = ['black','blue', 'blue','red', 'red', 'darkmagenta', 'darkmagenta']  
models = ['MockPS', 'FZH', 'FZHinv', 'F_MHR', 'F_MHRinv', 'MHR', 'MHRinv']
"""
#j_tscopes
models = ['MockPS_LOFAR', 'MockPS_HERA61', 'MockPS_SKA', 'MockPS']

#for j ps
Lstyle = ['-','--', '-', '--', '-', '--']
colour = ['blue', 'blue','red', 'red', 'darkmagenta', 'darkmagenta']  
models = ['FZH', 'FZHinv', 'F_MHR', 'F_MHRinv', 'MHR', 'MHRinv']
"""


#?##'MockPS_HERA64', 'MockPS_LOFAR',
##'S_MHR', 'S_MHRinv',
#
#models = ['MockPS_f3', 'MHR', '1p_MHR']#, '1p_MHR(z)']

NFmodels = models[1:]

def Driver(params, redshift, model):
 	SIXPLACES = Decimal(10) ** -6
	separator = " "
	seq = []
	seq.append('Redshift')
	for i in range(len(params)):    
		seq.append("%s"%(Decimal(repr(params[i])).quantize(SIXPLACES)))
	for i in range(len(redshift)):
	# Run 21CMMC (the 21cmFAST code) to perform the reionisation simulation and recover the neutral fraction and 21cm PS

		Individual_ID = redshift[i] #here I use a redshift id thread rather than a random one (like in the actual streamlined driver)
		seq[0] = ("z%s"%(Individual_ID))


		StringArgument = string.join(seq,separator)
		
		if (model == 'FZH'):
			command = "./21cmFAST/drive_21cmMC_streamlined %g %s 1"%(redshift[i],StringArgument) #run the driver from this file's location
		if (model == 'MHR'):	
			command = "./21cmFAST/drive_MHR_streamlined %g %s 1"%(redshift[i],StringArgument) 
		if (model == 'FZHinv'):
			command = "./21cmFAST/drive_FZHinv_streamlined %g %s 1"%(redshift[i],StringArgument) 	
		if (model == 'MHRinv'):
			command = "./21cmFAST/drive_MHRinv_streamlined %g %s 1"%(redshift[i],StringArgument)
			
		#command = "./21cmFAST/ComputingTau_e	
		print command
		os.system(command)

if (Telescope == 'LOFAR48' or Telescope=='LOFAR-DRIFT'):
	ParamsF = ParamsF_lofar
	ParamsFi = ParamsFi_lofar
	ParamsM = ParamsM_lofar
	ParamsMi = ParamsMi_lofar
	ParamsM2 = ParamsM2_lofar
	ParamsMi2 =ParamsMi2_lofar

if (Telescope == 'SKA512'):
	ParamsF = ParamsF_ska
	ParamsFi = ParamsFi_ska
	ParamsM = ParamsM_ska
	ParamsMi = ParamsMi_ska
	ParamsMi2 = ParamsMi2_ska
	ParamsM2 = ParamsM2_ska

# these give 0.5 NF (1sf)
"""
ParamsF = [0.203025231508187787E+02, 0.195698785005491196E+02, 0.443977183902213834E+01]
ParamsFi = [12.,0.999,4.]
ParamsM = [10., 3.499, 4.0]
ParamsMi = [10., 3.501, 4.0]  
ParamsMi2 = [0.49999] #0.5 but silly thing overwrites itself as the model name is in the deltps string
ParamsM2 = [0.50001]
"""
#ParamsM = [30., 4.5]
#ParamsM = [0.5]
if Drive:
	Driver(ParamsF, Redshift, 'FZH')
	Driver(ParamsFi, Redshift, 'FZHinv')
	Driver(ParamsM, Redshift, 'MHR')
	Driver(ParamsMi, Redshift, 'MHRinv')
	Driver(ParamsM2, Redshift, 'MHR')
	Driver(ParamsMi2, Redshift, 'MHRinv')

"""
#ParamsM = [40., 5.]
#Driver(M2, Redshift, 'MHR')

#ParamsM = [30.,15.,5.]
#Driver(ParamsM2, Redshift, 'MHR')
#PS error spline from Liklihood21cmFast.py (copied for reference)

k_values_estimate = dataF[:,0]
PS_values_estimate = dataF[:,1]

k_values = np.loadtxt("inputData/NoiseData/MockObs_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0.txt"%(redshifts[j]), usecols=[0]) # answers
PS_values = np.loadtxt("inputData/NoiseData/MockObs_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0.txt"%(redshifts[j]), usecols=[1]) #   

Error_k_values = np.loadtxt("inputData/NoiseData/TotalError_HERA331_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0.txt"%(redshifts[j]), usecols=[0]) # Tscoperrors
PS_Error = np.loadtxt("inputData/NoiseData/TotalError_HERA331_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0.txt"%(redshifts[j]), usecols=[1]) #   

PSError_Spline = interpolate.splrep(Error_k_values,np.log10(PS_Error),s=0)
MockPS_Spline = interpolate.splrep(k_values,np.log10(PS_values),s=0)

MockPS_val = 10**(interpolate.splev(k_values_estimate,MockPS_Spline,der=0))
ErrorPS_val = 10**(interpolate.splev(k_values_estimate,PSError_Spline,der=0))
"""



#MHR 
#Driver(ParamsM2 , Redshift, 'MHR')
#Driver(ParamsM1 , Redshift, 'MHR')
#Driver(ParamsM1z , Redshift, 'MHR')

#print 'first driver 3 params argc= 7'

#Driver(Params, Redshift, 'MHRinv')


#print '\n \n \n 2nd driver 2 params argc= 6 '

#Driver(p2, Redshift, 'MHRinv')

#labels = ['(a)', '(b)', '(c)']

# for plotting models per redshift

SIXPLACES = Decimal(10) ** -6
separator = "_"

seqF = []
seqFi = []
seqF.append('Redshift')
seqFi.append('Redshift')

seqM = []
seqMi = []
seqM.append('Redshift')
seqMi.append('Redshift')

seqIM = []
seqIMi = []
seqIM.append('Redshift')
seqIMi.append('Redshift')
seqM1z = []
seqM1z.append('Redshift')

seqM1 = []
seqM1.append('Redshift')

NF_F = np.zeros(len(Redshift))
NF_Fi = np.zeros(len(Redshift))
NF_M = np.zeros(len(Redshift))
NF_Mi = np.zeros(len(Redshift))
NF_IM = np.zeros(len(Redshift))
NF_IMi = np.zeros(len(Redshift))

for i in range(3):    
	seqF.append("%s"%(Decimal(repr(ParamsF[i])).quantize(SIXPLACES)))
	seqFi.append("%s"%(Decimal(repr(ParamsFi[i])).quantize(SIXPLACES)))
	seqIM.append("%s"%(Decimal(repr(ParamsM[i])).quantize(SIXPLACES)))
	seqIMi.append("%s"%(Decimal(repr(ParamsMi[i])).quantize(SIXPLACES)))
	#seqM1z.append("%s"%(Decimal(repr(ParamsM1z[i])).quantize(SIXPLACES)))
for i in range(2):  #1 #2
	seqM.append("%s"%(Decimal(repr(ParamsM2[i])).quantize(SIXPLACES)))
	seqMi.append("%s"%(Decimal(repr(ParamsMi2[i])).quantize(SIXPLACES)))
#for i in range(1):    
#	seqM1.append("%s"%(Decimal(repr(ParamsM1[i])).quantize(SIXPLACES)))
#	#seqM1i.append("%s"%(Decimal(repr(ParamsM1i[i])).quantize(SIXPLACES)))

 # plotting different model PS
for i in range(len(Redshift)):
	Individual_ID = Redshift[i] 
	seqF[0] = ("z%s"%(Individual_ID))
	seqFi[0] = ("z%s"%(Individual_ID))
	seqM[0] = ("z%s"%(Individual_ID))
	seqMi[0] = ("z%s"%(Individual_ID))
	seqIM[0] = ("z%s"%(Individual_ID))
	seqIMi[0] = ("z%s"%(Individual_ID))
	seqM1[0] = ("z%s"%(Individual_ID))
	seqM1z[0] = ("z%s"%(Individual_ID))


	StringArgumentFi = string.join(seqFi,separator)
	StringArgumentF = string.join(seqF,separator)
	StringArgumentMi = string.join(seqMi,separator)
	StringArgumentM = string.join(seqM,separator)
	StringArgumentIMi = string.join(seqIMi,separator)
	StringArgumentIM = string.join(seqIM,separator)
	StringArgumentM1z = string.join(seqM1z,separator)
	StringArgumentM1 = string.join(seqM1,separator)


	string_nameFi = 'delTps_estimate_%s'%(StringArgumentFi)
	string_nameF = 'delTps_estimate_%s'%(StringArgumentF)
	string_nameMi = 'delTps_estimate_%s'%(StringArgumentMi)
	string_nameM = 'delTps_estimate_%s'%(StringArgumentM)
	string_nameIMi = 'delTps_estimate_%s'%(StringArgumentIMi)
	string_nameIM = 'delTps_estimate_%s'%(StringArgumentIM)
	string_nameM1 = 'delTps_estimate_%s'%(StringArgumentM1)
	string_nameM1z = 'delTps_estimate_%s'%(StringArgumentM1z)


	for j in range(len(models)):
		
		if (models[j] == 'MockPS'):
			
			#print string_nameF
			dataF = np.loadtxt(string_nameF + '.txt')#
	
			#full_ErrorDat = np.loadtxt('HERA331_noise/TotalError_HERA331_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_1000hr.txt'%(redshifts[i]))
			#y_newErrors = interpolatehatch="/".splrep(full_ErrorDat[:,0], np.log10(full_ErrorDat[:,1]), dataF[:,0])
			#np.savetxt('HERA331_noise_%s'%(redshifts[i]), y_newErrors)			
			
			k_values_estimate = dataF[:,0]
			PS_values_estimate =dataF[:,1]

			k_values = np.loadtxt("inputData/NoiseData/MockObs_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0.txt"%(redshifts[i]), usecols=[0]) # answers
			PS_values = np.loadtxt("inputData/NoiseData/MockObs_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0.txt"%(redshifts[i]), usecols=[1]) #   

			Error_k_values = np.loadtxt("inputData/NoiseData/TotalError_%s_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_%s.txt"%(Telescope, redshifts[i],Obs_time), usecols=[0]) # Tscoperrors
			PS_Error = np.loadtxt("inputData/NoiseData/TotalError_%s_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_%s.txt"%(Telescope, redshifts[i],Obs_time), usecols=[1]) #   


			PSError_Spline = interpolate.splrep(Error_k_values,np.log10(PS_Error),s=0)   
			MockPS_Spline = interpolate.splrep(k_values,np.log10(PS_values),s=0)

			MockPS_val = 10**(interpolate.splev(k_values_estimate,MockPS_Spline,der=0))
			ErrorPS_val = 10**(interpolate.splev(k_values_estimate,PSError_Spline,der=0))#10**

			
			
			#print len(PS_Error), len(k_values), len(ErrorPS_val), len(PS_values), len(PSError_Spline)
			plt.errorbar(k_values[3:], PS_values[3:],  marker = '.',linestyle = '-', color =  'black', label ='HERA331')#colour[j], label = models[j])#yerr=ErrorPS_val, color = 'blue'
			plt.fill_between(k_values[3:], PS_values[3:]-ErrorPS_val, PS_values[3:]+ErrorPS_val, color='grey', alpha=0.6)

		if (models[j] == 'MockPS_SKA'):
			
			#print string_nameF
			dataF = np.loadtxt(string_nameF + '.txt')#
	
			#full_ErrorDat = np.loadtxt('HERA331_noise/TotalError_HERA331_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_1000hr.txt'%(redshifts[i]))
			#y_newErrors = interpolate.splrep(full_ErrorDat[:,0], np.log10(full_ErrorDat[:,1]), dataF[:,0])
			#np.savetxt('HERA331_noise_%s'%(redshifts[i]), y_newErrors)			
			
			k_values_estimate = dataF[:,0]
			PS_values_estimate =dataF[:,1]

			k_values = np.loadtxt("inputData/NoiseData/MockObs_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0.txt"%(redshifts[i]), usecols=[0]) # answers
			PS_values = np.loadtxt("inputData/NoiseData/MockObs_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0.txt"%(redshifts[i]), usecols=[1]) #   

			Error_k_values = np.loadtxt("inputData/NoiseData/TotalError_SKA512_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_%s.txt"%( redshifts[i],Obs_time), usecols=[0]) # Tscoperrors
			PS_Error = np.loadtxt("inputData/NoiseData/TotalError_SKA512_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_%s.txt"%( redshifts[i],Obs_time), usecols=[1]) #   


			PSError_Spline = interpolate.splrep(Error_k_values,np.log10(PS_Error),s=0)   
			MockPS_Spline = interpolate.splrep(k_values,np.log10(PS_values),s=0)

			MockPS_val = 10**(interpolate.splev(k_values_estimate,MockPS_Spline,der=0))
			ErrorPS_val = 10**(interpolate.splev(k_values_estimate,PSError_Spline,der=0))#10**

			
			
			#print len(PS_Error), len(k_values), len(ErrorPS_val), len(PS_values), len(PSError_Spline)
			plt.errorbar(k_values[3:], PS_values[3:],  marker = '.',linestyle = '-', color = 'red', label = 'SKA512')#color = colour[j])#yerr=ErrorPS_val, 
			plt.fill_between(k_values[3:], PS_values[3:]-ErrorPS_val, PS_values[3:]+ErrorPS_val, color='red', alpha=0.5, hatch=3*"+")# 

		if (models[j] == 'MockPS_LOFAR'):
			
			#print string_nameF
			dataF = np.loadtxt(string_nameF + '.txt')#
	
			#full_ErrorDat = np.loadtxt('HERA331_noise/TotalError_HERA331_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_1000hr.txt'%(redshifts[i]))
			#y_newErrors = interpolate.splrep(full_ErrorDat[:,0], np.log10(full_ErrorDat[:,1]), dataF[:,0])
			#np.savetxt('HERA331_noise_%s'%(redshifts[i]), y_newErrors)			
			
			k_values_estimate = dataF[:,0]
			PS_values_estimate =dataF[:,1]

			k_values = np.loadtxt("inputData/NoiseData/MockObs_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0.txt"%(redshifts[i]), usecols=[0]) # answers
			PS_values = np.loadtxt("inputData/NoiseData/MockObs_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0.txt"%(redshifts[i]), usecols=[1]) #   

			Error_k_values = np.loadtxt("inputData/NoiseData/TotalError_LOFAR48_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_%s.txt"%( redshifts[i],Obs_time), usecols=[0]) # Tscoperrors
			PS_Error = np.loadtxt("inputData/NoiseData/TotalError_LOFAR48_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_%s.txt"%( redshifts[i],Obs_time), usecols=[1]) #   


			PSError_Spline = interpolate.splrep(Error_k_values,np.log10(PS_Error),s=0)   
			MockPS_Spline = interpolate.splrep(k_values,np.log10(PS_values),s=0)

			MockPS_val = 10**(interpolate.splev(k_values_estimate,MockPS_Spline,der=0))
			ErrorPS_val = 10**(interpolate.splev(k_values_estimate,PSError_Spline,der=0))#10**
			
			#print len(PS_Error), len(k_values), len(ErrorPS_val), len(PS_values), len(PSError_Spline)
			plt.errorbar(k_values[3:], PS_values[3:],  marker = '.',linestyle = '-', color = 'lightgrey', label = 'LOFAR48')#color = colour[j])#yerr=ErrorPS_val, 
			plt.fill_between(k_values[3:], PS_values[3:]-ErrorPS_val, PS_values[3:]+ErrorPS_val, color='black', alpha=0.08)


		if (models[j] == 'MockPS_HERA61'):
			
			#print string_nameF
			dataF = np.loadtxt(string_nameF + '.txt')#
	
			#full_ErrorDat = np.loadtxt('HERA331_noise/TotalError_HERA331_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_1000hr.txt'%(redshifts[i]))
			#y_newErrors = interpolate.splrep(full_ErrorDat[:,0], np.log10(full_ErrorDat[:,1]), dataF[:,0])
			#np.savetxt('HERA331_noise_%s'%(redshifts[i]), y_newErrors)			
			
			k_values_estimate = dataF[:,0]
			PS_values_estimate =dataF[:,1]

			k_values = np.loadtxt("inputData/NoiseData/MockObs_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0.txt"%(redshifts[i]), usecols=[0]) # answers
			PS_values = np.loadtxt("inputData/NoiseData/MockObs_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0.txt"%(redshifts[i]), usecols=[1]) #   

			Error_k_values = np.loadtxt("inputData/NoiseData/TotalError_HERA61_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_%s.txt"%( redshifts[i],Obs_time), usecols=[0]) # Tscoperrors
			PS_Error = np.loadtxt("inputData/NoiseData/TotalError_HERA61_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_%s.txt"%( redshifts[i],Obs_time), usecols=[1]) #   


			PSError_Spline = interpolate.splrep(Error_k_values,np.log10(PS_Error),s=0)   
			MockPS_Spline = interpolate.splrep(k_values,np.log10(PS_values),s=0)

			MockPS_val = 10**(interpolate.splev(k_values_estimate,MockPS_Spline,der=0))
			ErrorPS_val = 10**(interpolate.splev(k_values_estimate,PSError_Spline,der=0))#10**

			
			
			#print len(PS_Error), len(k_values), len(ErrorPS_val), len(PS_values), len(PSError_Spline)
			plt.errorbar(k_values[3:], PS_values[3:],  marker = '.',linestyle = '-', color = 'darkgrey', label = 'HERA61')# color = colour[j])#yerr=ErrorPS_val, 
			plt.fill_between(k_values[3:], PS_values[3:]-ErrorPS_val, PS_values[3:]+ErrorPS_val, color='black', alpha=0.2)

		if (models[j] == 'MockPS_f3'):
			
			#print string_nameF
			dataM2 = np.loadtxt(string_nameM + '.txt')#
	
			#full_ErrorDat = np.loadtxt('HERA331_noise/TotalError_HERA331_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_1000hr.txt'%(redshifts[i]))
			#y_newErrors = interpolate.splrep(full_ErrorDat[:,0], np.log10(full_ErrorDat[:,1]), dataF[:,0])
			#np.savetxt('HERA331_noise_%s'%(redshifts[i]), y_newErrors)			
			
			k_values_estimate = dataM2[:,0]
			PS_values_estimate =dataM2[:,1]

			k_values = np.loadtxt("inputData/NoiseData/MockObs_PS_250Mpc_z%s_30.0_4.5.txt"%(redshifts[i]), usecols=[0]) # answers
			PS_values = np.loadtxt("inputData/NoiseData/MockObs_PS_250Mpc_z%s_30.0_4.5.txt"%(redshifts[i]), usecols=[1]) #   

			Error_k_values = np.loadtxt("inputData/NoiseData/TotalError_%s_PS_250Mpc_z%s_30.0_4.5_%s.txt"%(Telescope, redshifts[i],Obs_time), usecols=[0]) # Tscoperrors
			PS_Error = np.loadtxt("inputData/NoiseData/TotalError_%s_PS_250Mpc_z%s_30.0_4.5_%s.txt"%(Telescope, redshifts[i],Obs_time), usecols=[1]) #   

			#might Need '_1000hr.txt' at end of TtoalError files if using HERA

			PSError_Spline = interpolate.splrep(Error_k_values,np.log10(PS_Error),s=0)   
			MockPS_Spline = interpolate.splrep(k_values,np.log10(PS_values),s=0)

			MockPS_val = 10**(interpolate.splev(k_values_estimate,MockPS_Spline,der=0))
			ErrorPS_val = 10**(interpolate.splev(k_values_estimate,PSError_Spline,der=0))#10**

			
			
			#print len(PS_Error), len(k_values), len(ErrorPS_val), len(PS_values), len(PSError_Spline)
			plt.errorbar(k_values, PS_values, yerr=ErrorPS_val,  marker = '.',linestyle = '-', color = colour[j], label = models[j])
			

		if (models[j] == '1p_MHR'):
			
			dataM1 = np.loadtxt(string_nameM1 + '.txt')#
			plt.errorbar(dataM1[:,0], dataM1[:,1],  marker = '.',linestyle = '--', color = colour[j], label = models[j])
			NFstring_M1 = 'NeutralFraction_%s'%(StringArgumentM1)
			#NF_M1[i] = np.loadtxt(NFstring_M1 + '.txt')

		if (models[j] == '1p_MHR(z)'):
			
			dataM1z = np.loadtxt(string_nameM1z + '.txt')#
			plt.errorbar(dataM1z[:,0], dataM1z[:,1],  marker = '.',linestyle = '-', color = colour[j], label = models[j])
			NFstring_M1z = 'NeutralFraction_%s'%(StringArgumentM1z)
			#NF_M1z[i] = np.loadtxt(NFstring_M1z + '.txt')		

		if (models[j] == 'FZH'):
			
			dataF = np.loadtxt(string_nameF + '.txt')#
			plt.errorbar(dataF[:,0], dataF[:,1],  marker = '.',linestyle = '-', color = colour[j], label = models[j])
			NFstring_F = 'NeutralFraction_%s'%(StringArgumentF)
			NF_F[i] = np.loadtxt(NFstring_F + '.txt')	
		
		if (models[j] == 'FZHinv'):
			
			dataFi = np.loadtxt(string_nameFi + '.txt')#  'PS_MODELS/' + models[j]+ '/' +
			plt.errorbar(dataFi[:,0], dataFi[:,1], marker = '.', linestyle = '--', color = colour[j], label = None)
			NFstring_Fi = 'NeutralFraction_%s'%(StringArgumentFi)
			NF_Fi[i] = np.loadtxt(NFstring_Fi + '.txt')

		if (models[j] == 'MHR'):
			
			dataM = np.loadtxt(string_nameM + '.txt')# 'PS_MODELS/' + models[j]+ '/' +'Filtered ' +
			plt.errorbar(dataM[:,0], dataM[:,1], marker = '.',linestyle ='-', color = colour[j], label =  models[j])		
			NFstring_M = 'NeutralFraction_%s'%(StringArgumentM)
			NF_M[i] = np.loadtxt(NFstring_M + '.txt')

		
		if (models[j] == 'MHRinv'):
			
			dataMi = np.loadtxt(string_nameMi + '.txt')#'PS_MODELS/' + models[j]+ '/' +	'Filtered ' + 
			plt.errorbar(dataMi[:,0], dataMi[:,1], marker = '.', linestyle = '--', color = colour[j], label = None)
			NFstring_Mi = 'NeutralFraction_%s'%(StringArgumentMi)
			NF_Mi[i] = np.loadtxt(NFstring_Mi + '.txt')

		if (models[j] == 'F_MHR'):
			
			dataIM = np.loadtxt(string_nameIM + '.txt')# 'PS_MODELS/' + models[j]+ '/' +'Filtered ' +
			plt.errorbar(dataIM[:,0], dataIM[:,1], marker = '.',linestyle ='-', color = colour[j], label =  models[j])
			NFstring_IM = 'NeutralFraction_%s'%(StringArgumentIM)
			NF_IM[i] = np.loadtxt(NFstring_IM + '.txt')	
	
		if (models[j] == 'F_MHRinv'):
			
			dataIMi = np.loadtxt(string_nameIMi + '.txt')# 'PS_MODELS/' + models[j]+ '/' +'Filtered ' +
			plt.errorbar(dataIMi[:,0], dataIMi[:,1], marker = '.',linestyle ='--', color = colour[j], label =  None )
			NFstring_IMi = 'NeutralFraction_%s'%(StringArgumentIMi)
			NF_IMi[i] = np.loadtxt(NFstring_IMi + '.txt')

		plt.legend(loc = 4)
		plt.title('Redshift ' + str(Redshift[i]))	
		plt.xscale('log')
		plt.xlim((0.09,1.1))
		plt.ylim((1.,220))
		#plt.xaxis.set_label_coords(0.3, 0.8)
		plt.xlabel('k[$Mpc^{-1}$]', labelpad=-10)
		plt.ylabel('$\Delta^2(k)$ $\mathit{mK}^2$')
		plt.plot([1.,1.],[0.01,1000], "k--",linewidth = 0.1)
		plt.plot([0.15,0.15],[0.01,1000], "k--",linewidth = 0.1)
		#plt.plot([0.01,1000],[1.,1.], "k--",linewidth = 0.1)
		plt.yscale('log')
		title = 'Redshift_' + str(Redshift[i])	
		#plt.title(title)

	plt.savefig(title+'_%s_%s_'%(Telescope,Obs_time) + nametag +'.pdf')
	#plt.savefig(title+'_%s_ALL_Tscope.pdf'%(Obs_time))
	#plt.savefig(title+'_half_NF.pdf')
	plt.close()



"""
############################################################	
##### NF Interpolation plots #####

nf_vals = NF_IM  #choose a model
Redshift = [8.,9.,10.]

####PLANCK COPIED FROM 21CMMC# (estimates optical depth)
PlanckTau_Mean = 0.058
PlanckTau_OneSigma = 0.012

nZinterp = 15

ZExtrap_min = 5.9
ZExtrap_max = 18.0
ZExtrapVals = np.zeros(nZinterp)
XHI_ExtrapVals = np.zeros(nZinterp)
Individual_ID = Decimal(1).quantize(SIXPLACES)  #from 21CMMC

# Perform only a linear interpolation/extrapolation
LinearInterpolationFunction = InterpolatedUnivariateSpline(Redshift, nf_vals, k=1)

separator_Planck = " "
seq_Planck = []
for i in range(nZinterp):
	ZExtrapVals[i] = ZExtrap_min + (ZExtrap_max - ZExtrap_min)*float(i)/(nZinterp - 1)
        XHI_ExtrapVals[i] = LinearInterpolationFunction(ZExtrapVals[i])
	if XHI_ExtrapVals[i] > 1.0:
        	XHI_ExtrapVals[i] = 1.0
        if XHI_ExtrapVals[i] < 0.0:
        	XHI_ExtrapVals[i] = 0.0
	seq_Planck.append("%s"%(ZExtrapVals[i])) 
	seq_Planck.append("%s"%(XHI_ExtrapVals[i]))    

StringArgument_Planck = string.join(seq_Planck,separator_Planck)
command = './21cmFAST/ComputingTau_e %s %s %s'%(Individual_ID,Decimal(repr(ParamsF[0])).quantize(SIXPLACES),StringArgument_Planck)
os.system(command)
tau_value = np.loadtxt('Tau_e_%s_%s.txt'%(Individual_ID,Decimal(repr(ParamsF[0])).quantize(SIXPLACES)), usecols=(0,))

print 'Planck: ',tau_value, PlanckTau_Mean, -0.5*np.square( ( PlanckTau_Mean - tau_value )/(PlanckTau_OneSigma) )

##### McGreer
print 'McGreer: '
McGreer_Mean = 0.06
McGreer_OneSigma = 0.05            
McGreer_Redshift = 5.9
if McGreer_Redshift in Redshift:    #5.9
	for i in range(len(Redshift)):
                    if Redshift[i] == McGreer_Redshift:                        
                        McGreer_NF = nf_vals[i]
        if McGreer_NF > 1.:
        	McGreer_NF = 1.
	if McGreer_NF < 0.:
        	McGreer_NF = 0.
	if McGreer_NF <= 0.06:
                    print 0.0, 'exit1' # Add zero, as we assume flat (unity) probability at x_HI <= 0.06 (as it is a lower limit)
        else:
                    print -0.5*np.square( ( McGreer_Mean - McGreer_NF )/(McGreer_OneSigma) ), 'exit 2'
elif len(Redshift) > 2:
	LinearInterpolationFunction = InterpolatedUnivariateSpline(Redshift, nf_vals, k=1)
	McGreer_NF = LinearInterpolationFunction(McGreer_Redshift)
	if McGreer_NF > 1.:
		McGreer_NF = 1.
	if McGreer_NF < 0.:
		McGreer_NF = 0.
	if McGreer_NF <= 0.06:
		print 0.0, 'exit 3' # Add zero, as we assume flat (unity) probability at x_HI <= 0.06 (as it is a lower limit)
	else:
		print -0.5*np.square(( McGreer_Mean - McGreer_NF )/(McGreer_OneSigma) ), 'exit 4'


##### Grieg
print 'Greig: '
NFVals_QSO = []
PDFVals_QSO = []
with open('inputData/PriorData/NeutralFractionsForPDF.out','rb') as handle:
	NFValsQSO = pickle.loads(handle.read())

with open('inputData/PriorData/NeutralFractionPDF_SmallHII.out','rb') as handle:
	PDFValsQSO = pickle.loads(handle.read())
normalisation = np.amax(PDFValsQSO)
PDFValsQSO = PDFValsQSO/normalisation
QSO_Redshift = 7.0842

spline_QSODampingPDF = interpolate.splrep(NFValsQSO, PDFValsQSO, s=1)

if QSO_Redshift in Redshift:
	for i in range(len(Redshift)):
		if Redshift[i] == QSO_Redshift:                        
			NF_QSO = nf_vals[i]
	if NF_QSO > 1.0:
		NF_QSO = 1.0
	if NF_QSO < 0.0:
		NF_QSO = 0.0
	
	QSO_Prob = interpolate.splev(NF_QSO,spline_QSODampingPDF,der=0)
        
	if QSO_Prob <= 0.0:
		QSO_Prob = 0.000006           

        print  np.log(QSO_Prob), 'exit 1'

elif len(Redshift) > 2:
	
	if QSO_Redshift < np.amin(Redshift):
                   
		LinearInterpolationFunction = InterpolatedUnivariateSpline(Redshift, nf_vals, k=1)

		NF_QSO = LinearInterpolationFunction(QSO_Redshift)
                            
	else:
		spline_reionisationhistory = interpolate.splrep(Redshift,nf_vals,s=0)

		NF_QSO = interpolate.splev(QSO_Redshift,spline_reionisationhistory,der=0)
	print NF_QSO
                # Ensure that the neutral fraction does not exceed unity, or go negative
	if NF_QSO > 1.0:
		NF_QSO = 1.0
	if NF_QSO < 0.0:
		NF_QSO = 0.0

	QSO_Prob = interpolate.splev(NF_QSO,spline_QSODampingPDF,der=0)

               
	if QSO_Prob <= 0.0:
		QSO_Prob = 0.000006
                
        print np.log(QSO_Prob), 'exit 2'
	print 'nf_', NF_QSO, 'prob', QSO_Prob
"""
"""
#plt.plot(ZExtrapVals, XHI_ExtrapVals, label = 'FZH')



#TOM #
#	-- mark on z = 7.0842 ,  (spline_QSODampingPDF)
#	-- z = 5.9 (chi2 with 0.06 +- 0.05)
#
#	-- plot Tau_s  Tau_mean = 0.058 +- 0.012

"""

"""
for j in range(len(NFmodels)):
	#if (models[j] == 'MockPS'):
	
	if (NFmodels[j] == 'FZH'):
		plt.plot(redshifts, NF_F, color = 'blue', label = NFmodels[j])

	if (NFmodels[j] == 'FZHinv'):
		plt.plot(redshifts, NF_Fi, linestyle ='--', color = 'blue', label = NFmodels[j])

	if (NFmodels[j] == 'MHR'):
		plt.plot(redshifts, NF_M, color = 'black', label = NFmodels[j])

	if (NFmodels[j] == 'MHRinv'):
		plt.plot(redshifts, NF_Mi, linestyle ='--', color = 'black', label = NFmodels[j])

	if (NFmodels[j] == 'F_MHR'):
		plt.plot(redshifts, NF_IM, color = 'red', label = NFmodels[j])

	if (NFmodels[j] == 'F_MHRinv'):
		plt.plot(redshifts, NF_IMi, linestyle ='--', color = 'red', label = NFmodels[j])


plt.title('Neutral Fraction Progression with Redshift')
plt.xlabel('$z$')
plt.ylabel('$x_{HI}$')
plt.legend(loc = 7)
plt.savefig('NF_interpolation_LOFAR.png')

nf = np.zeros((6,3))

nf[0,:] = NF_F
nf[1,:] = NF_Fi
nf[2,:] = NF_M	  #2p
nf[3,:] = NF_Mi  
nf[4,:] = NF_IM   #3p
nf[5,:] = NF_IMi

np.savetxt('NF_LOFAR.txt', nf)
"""


"""
#redshifts per model
SIXPLACES = Decimal(10) ** -6
separator = "_"
seq = []
seq.append('Redshift')


#####do the sting_nameF from above....

for j in range(len(models)):	
	for i in range(0,3,1):
		Individual_ID = Redshift[i] 
		seq = []
		seq.append('Redshift')	
		
		
		if (models[j] == 'MockPS'):
			#singleZDriver(ParamsF, Redshift[i] ,'FZH' )
			dataF = np.loadtxt(string_nameF + '.txt')#
	
			#full_ErrorDat = np.loadtxt('HERA331_noise/TotalError_HERA331_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_1000hr.txt'%(redshifts[i]))
			#y_newErrors = interpolate.splrep(full_ErrorDat[:,0], np.log10(full_ErrorDat[:,1]), dataF[:,0])
			#np.savetxt('HERA331_noise_%s'%(redshifts[i]), y_newErrors)			
			
			k_values_estimate = dataF[:,0]
			PS_values_estimate =dataF[:,1]

			k_values = np.loadtxt("inputData/NoiseData/MockObs_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0.txt"%(redshifts[j]), usecols=[0]) # answers
			PS_values = np.loadtxt("inputData/NoiseData/MockObs_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0.txt"%(redshifts[j]), usecols=[1]) #   

			Error_k_values = np.loadtxt("inputData/NoiseData/TotalError_HERA331_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_1000hr.txt"%(redshifts[j]), usecols=[0]) # Tscoperrors
			PS_Error = np.loadtxt("inputData/NoiseData/TotalError_HERA331_PS_250Mpc_z%s_20.0_15.0_30000.0_0.0_1000hr.txt"%(redshifts[j]), usecols=[1]) #   

			PSError_Spline = interpolate.splrep(Error_k_values,np.log10(PS_Error),s=0)   
			MockPS_Spline = interpolate.splrep(k_values,np.log10(PS_values),s=0)

			MockPS_val = 10**(interpolate.splev(k_values_estimate,MockPS_Spline,der=0))
			ErrorPS_val = 10**(interpolate.splev(k_values_estimate,PSError_Spline,der=0))#10**
			#yerr = ErrorPS_val,
			plt.errorbar(k_values, PS_values, yerr=PS_Error,  marker = '.',linestyle = '-', color = colour[j], label = models[j])

				
		if (models[j] == 'FZH'):
			#define ParamsF at the top for best fit u want to use.
			for k in range(len(ParamsF)):    
				seq.append("%s"%(Decimal(repr(ParamsF[k])).quantize(SIXPLACES)))
				seq[0] = ("z%s"%(Individual_ID))
				StringArgument = string.join(seq,separator)
				string_name = 'delTps_estimate_%s'%(StringArgument)
			data = np.loadtxt(string_name + '.txt')#
			labelF = str(models[j])+'_z' + str(Redshift[i])
			plt.errorbar(data[:,0], data[:,1], yerr = data[:,2], marker = 'v',linestyle = Lstyle[i], color = colour[j], label = labelF)

			
		if (models[j] == 'FZHinv'):
			#define ParamsFi at the top for best fit u want to use.			
			for k in range(len(ParamsFi)):    
				seq.append("%s"%(Decimal(repr(ParamsFi[k])).quantize(SIXPLACES)))
				seq[0] = ("z%s"%(Individual_ID))
				StringArgument = string.join(seq,separator)
				string_name = 'delTps_estimate_%s'%(StringArgument)
			data = np.loadtxt( string_name + '.txt') 
			plt.errorbar(data[:,0], data[:,1], yerr = data[:,2], marker = '*', linestyle = Lstyle[i], color = colour[j], label = str(models[j])+'_z' + str(Redshift[i]))
		if (models[j] == 'MHR'):
			#define ParamsF at the top for best fit u want to use.
			for k in range(len(Params)):    
				seq.append("%s"%(Decimal(repr(Params[k])).quantize(SIXPLACES)))
				seq[0] = ("z%s"%(Individual_ID))
				StringArgument = string.join(seq,separator)
				string_name = 'delTps_estimate_%s'%(StringArgument)
			data = np.loadtxt(string_name + '.txt')#
			labelM = str(models[j])+'_z' + str(Redshift[i])
			plt.errorbar(data[:,0], data[:,1], yerr = data[:,2], marker = 'v',linestyle = Lstyle[i], color = colour[j], label = labelM)
		plt.legend(loc = 4)
plt.xscale('log')
#plt.xlim((0.05,1.3))
plt.plot([1.,1.],[0.01,100], "k--",linewidth = 0.1)
plt.plot([0.1,0.1],[0.01,100], "k--",linewidth = 0.1)
plt.plot([0.01,100],[1.,1.], "k--",linewidth = 0.1)
plt.yscale('log')
#plt.ylim((0.5,110))
title = 'MockPS_allz'
plt.xlabel('k[$Mpc^{-1}$]')
plt.ylabel('$\Delta^2(k)$')
#plt.title(title)
plt.savefig(title+'.png')
plt.close()

#print Params

#For plotting MHR 2p against MHR 3p

SIXPLACES = Decimal(10) ** -6
separator = "_"

Params3M = ParamsM
Params2M = ParamsM2#[:][0::2]
seq3M = []
seq2M = []
seq3M.append('Redshift')
seq2M.append('Redshift')

Params3Mi = ParamsMi
Params2Mi = ParamsMi2#[:][0::2]
seq3Mi = []
seq2Mi = []
seq3Mi.append('Redshift')
seq2Mi.append('Redshift')

for i in range(len(Params)):    
	seq3M.append("%s"%(Decimal(repr(Params3M[i])).quantize(SIXPLACES)))
	seq3Mi.append("%s"%(Decimal(repr(Params3Mi[i])).quantize(SIXPLACES)))
for i in range(len(Params)-1):
	seq2M.append("%s"%(Decimal(repr(Params2M[i])).quantize(SIXPLACES)))
	seq2Mi.append("%s"%(Decimal(repr(Params2Mi[i])).quantize(SIXPLACES)))
 # plotting different model PS
for i in range(len(Redshift)):
	Individual_ID = Redshift[i] 
	
	seq3M[0] = ("z%s"%(Individual_ID))
	seq2M[0] = ("z%s"%(Individual_ID))
	seq3Mi[0] = ("z%s"%(Individual_ID))
	seq2Mi[0] = ("z%s"%(Individual_ID))
	StringArgument3M = string.join(seq3M,separator)
	StringArgument2M = string.join(seq2M,separator)
	StringArgument3Mi = string.join(seq3Mi,separator)
	StringArgument2Mi = string.join(seq2Mi,separator)
	string_name3M = 'delTps_estimate_%s'%(StringArgument3M)
	string_name2M = 'delTps_estimate_%s'%(StringArgument2M)
	string_name3Mi = 'delTps_estimate_%s'%(StringArgument3Mi)
	string_name2Mi = 'delTps_estimate_%s'%(StringArgument2Mi)
	

		
	singleZDriver(Params3M, Redshift[i] , 'MHR')
	data3M = np.loadtxt(string_name3M + '.txt')# 'PS_MODELS/' + models[j]+ '/' +
	plt.errorbar(data3M[:,0], data3M[:,1], yerr = data3M[:,2], marker = '.',linestyle ='--', color = 'blue', label = 'Filtered MHR')
			
	singleZDriver(Params2M, Redshift[i] , 'MHR')
	data2M = np.loadtxt(string_name2M + '.txt')# 'PS_MODELS/' + models[j]+ '/' +
	plt.errorbar(data2M[:,0], data2M[:,1], yerr = data2M[:,2], marker = '.',linestyle ='-', color = 'blue', label = 'MHR')
		
	singleZDriver(Params3Mi, Redshift[i] , 'MHRinv')
	data3Mi = np.loadtxt(string_name3Mi + '.txt')# 'PS_MODELS/' + models[j]+ '/' +
	plt.errorbar(data3Mi[:,0], data3Mi[:,1], yerr = data3Mi[:,2], marker = '.',linestyle ='--', color = 'red', label ='Filtered MHRinv')
			
	singleZDriver(Params2Mi, Redshift[i] , 'MHRinv')
	data2Mi = np.loadtxt(string_name2Mi + '.txt')# 'PS_MODELS/' + models[j]+ '/' +
	plt.errorbar(data2Mi[:,0], data2Mi[:,1], yerr = data2Mi[:,2], marker = '.',linestyle ='-', color = 'red', label = 'MHRinv')
	
	plt.legend(loc = 4)
	#plt.title(string_name)	
	plt.xscale('log')
	plt.xlim((0.05,1.3))
	plt.ylim((0.1,500))
	plt.xlabel('k[$Mpc^{-1}$]')
	plt.ylabel('$\Delta^2(k)$')
	plt.plot([1.,1.],[0.01,1000], "k--",linewidth = 0.1)
	plt.plot([0.1,0.1],[0.01,1000], "k--",linewidth = 0.1)
	plt.plot([0.01,1000],[1.,1.], "k--",linewidth = 0.1)
	plt.yscale('log')
	title = 'MHR_filter_compar_z' + str(Redshift[i])	
	#plt.title(title)
	plt.savefig(title+'.png')
	plt.close()

"""

#So the files this produces are 
#	'delTps_estimate_Redshift_p[i]'s' - which have 3 columns (these can be used for 'MockObs_PS_250Mpc_Z_p[i]'s' in the input data.
					#column1:   k_values_estimate
					#column2:   PS_values_estimate
					#column3:	PS_with a different normalization.. don't know why?

#	'NeutralFraction_Redshift_p[i]'s - contains the NF at that redshift 
#

# In order to use these as the fiducial sets, rename with 'MockObs_PS_250Mpc_z#z_#p[0]_#p[1]_#10.^p[3]_#p[4].txt' and put in the inputData/NoiseData folder
#	(where #n is the value of n)

#To use a box size that isnt 250Mpc or redshifts that aren't 8,9,10 21cmFAST must be used.



