import numpy
import math
from scipy import interpolate
from decimal import *
import string
import pickle
import pymultinest
import emcee
from emcee.utils import MPIPool 
import matplotlib.pyplot as plot
import os
import sys

TWOPLACES = Decimal(10) ** -2       # same as Decimal('0.01')
FOURPLACES = Decimal(10) ** -4       # same as Decimal('0.0001')
SIXPLACES = Decimal(10) ** -6       # same as Decimal('0.000001')

from likelihood.LikelihoodComputationChain import *
from likelihood.Likelihood21cmFast import * 

# Parameter ranges for ionizing efficiency, mean free path, log_10(virial temperature) and alpha respectively.
# each array is in the form [mean,minimum,maximum,variance], where min/max are the uniform prior limits and mean and var are ONLY involved if the GaussianPrior is used (both samplers at the bottom is using 'prior' - just the uniform part.)
# If Includealpha is False, then we only consider the three parameter model (constant ionising efficiency) for the streamlined 21cmFAST	semi-numerical simulation.	
# If different parameters are used the methodology for the sampler has changed.. just make sure the fiducial values don't find the wrong data set (units aren't in the file names).

#Params = [[50.,5.,1000.,20.],[10.,5.,20.,3.],[numpy.log10(80000.),4.0,6.0,numpy.log10(50000.)],[0.,-3.,3.,2.]] #typical Big prior for biasing

#Params = [[50.,20.-0.005*numpy.sqrt(10.),20.+0.005*numpy.sqrt(10.),20.],[10.,15.-0.005*numpy.sqrt(10.),15.+0.005*numpy.sqrt(10.),3.],[numpy.log10(80000.),4.476,4.478,numpy.log10(50000.)],[0.,-3.,3.,2.]] #typical small prior for biasing

Params = [[50.,5.,250.,20.],[15.,5.,20.,0.],[numpy.log10(80000.),numpy.log10(10000.),numpy.log10(200000.),numpy.log10(50000.)],[0.,-3.,3.,2.]] #usedvalues(FZH)  (R_mfp is fixed on the fiducial)

#Params = [[50.,15.,15.,20.],[15.,15.,15.,0.],[numpy.log10(80000.),4.669,4.669,numpy.log10(50000.)],[0.,0.4,0.4,2.]]

#Params = [[50.,20.,20.,20.],[15.,15.,15.,0.],[numpy.log10(80000.),4.447,4.447,numpy.log10(50000.)],[0.,0.0,0.0,2.]]

#Params = [[50.,5.,1200.,20.],[10.,0.1,10.,3.],[numpy.log10(80000.),4.,6.,numpy.log10(50000.)],[0.,-3.,3.,2.]] #MHRinvs

#Params = [[50.,5.,4000.,20.],[10.,0.1,10.,3.],[numpy.log10(80000.),4.,7.,numpy.log10(50000.)],[0.,-3.,3.,2.]]
#MHRs

#Params = [[50.,5.,5000.,20.],[10.,0.1,10.,3.],[numpy.log10(80000.),4.,7.,numpy.log10(50000.)],[0.,-3.,3.,2.]]
#FZHinv


redshift = [8.,9.,10.]  #Choose which redshifts to interpolate over here (paramaters are assumed to be redshift independent, this code has boxes for 6,7,8,9,10 - use 21cmFAST if more are required)
ParamsZdependency = False    #only works with 'True' or 'False' as strings.
#allow parameters to vary with redshift ... ndim-->ndim*number of redshifts.

#Fiducial model parameters are [zeta, R_mfp, log_10{Tvir}, alpha]. 
#(21cmFAST and FHZinv)  ---> these specify which PS files to fit against by the string in the file name: 

#	1. --> p = [20,15,4.477,0.0] set by the 3 parameter FHZ model // 	
#	2. --> p = [15,15,4.699,0.4] set by the 4 parameter FHZ model.

#NEW 3. --> p =[30.,4.5] fiducial 2p MHRinv model (same morphology as f1 and 2 but local)

# (these values is Only used if there is a saved error file with the param values as the name (look below in the code for more details))

Fiducial_set=1. 

#Use 'Multinest' or 'Emcee'?

sampler = 'Multinest'# 

#In-out or Out-in and local or global models for likelihood calculations, options are: '21cmFAST', 'FZHinv', 'MHR' or 'MHRinv'.

Model_flag =  '21cmFAST'

Telescope = 'HERA331'#'HERA331'  #or 'SKA512' or 'LOFAR48' 
#Alternate dipole numbers for HERA are 19,61,127,217, and 469.  


Observing_time = '1080hrs' 
#HERA-N = '1080hrs' 
#LOFAR48:'1080hrs' or '2160hrs' or '4320hrs' or '10800hrs' or '21600hrs' (only f1 for these)
#SKA866 or SKA512-core: '1080hrs'(only f1 for these)
#SKA512: '1080hrs' or '540hrs' or '324hrs' or '216hrs' or '108hrs'(only f1 for these)

IncludeAlpha = False   # Only relevent to the FZHinv or 21cmFAST models


MHR_NFonly = False
NFParam = [[0.5,0.,1.,1.]]

FilterMHR = False	#relevent if using MHR or MHRinv ONLY: 	Enables the switching of a pixel-by-pixel implementation of the MHR (a 2 parameter model [\zeta, log10[T_vir]]) or enables a 3rd parameter R which is a filtering scale before implementing MHR [\zeta, R, log10[T_vir]]], this will enable comparisons that will help us deside the importance of Occam's razor within our Bayesian evidence calculation.  NOTE: MHR_NFonly and FilterMHR should not both be True at the same time.
				
	####################################################################################################################################
##############---- basic use of multinest or Emcee on 3pFZHf1,3pf2,4pf1,4pf2, and further models can be entirely specified byt the above global variables ----##############
	####################################################################################################################################

if (Model_flag == 'MHR' or Model_flag == 'MHRinv'):
	IncludeAlpha = False
	if not MHR_NFonly:
		if not FilterMHR:
			ndim=2 #Z, T 
			params = Params[:][0::2]
		if FilterMHR:
			ndim=3
			params = Params[:][:3]
		
	if MHR_NFonly:
		FilterMHR = False
		ndim=1 #NF = 1 - Zf_coll
		params = NFParam	

if (Model_flag ==  '21cmFAST' or Model_flag == 'FZHinv'): 
	FilterMHR = False
	MHR_NFonly = False	
	if not IncludeAlpha: 	
		ndim=3 	#Z, R, T,
		params = Params[:][:3]#
		
	if IncludeAlpha: 	
		ndim=4	# Z, R, T, A 
		params = Params

NFflag_for1pZdepMHR = False
if (ParamsZdependency == True):
	if not MHR_NFonly:	
		params_R = Params[:][1]
		params_T = Params[:][2]
		params_Z = Params[:][0]
		params_A = Params[:][3]

		for i in range(0,len(redshift)-1,1):
			params.append(params_Z)

			if not (FilterMHR == False and (Model_flag == 'MHR' or Model_flag == 'MHRinv')):
				params.append(params_R)

			params.append(params_T)

			if (IncludeAlpha == True and (Model_flag == '21cmFAST' or Model_flag == 'FZHinv')):
				params.append(params_A)

	if MHR_NFonly:
		for i in range(0,len(redshift)-1,1):
			params.append(NFParam[:][0])
		NFflag_for1pZdepMHR = True	
	ndim = ndim * len(redshift)	

#params = [[0.5, .1,1.0,0.3],[0.5, .1,1.0,0.3],[0.5, .1,1.0,0.3]]#trying to stop it clogging at 0??	

def show(filepath): 
	""" opens the output (pdf) file for the user """
	if os.name == 'mac' or platform == 'darwin': subprocess.call(('open', filepath))
	elif os.name == 'nt' or platform == 'win32': os.startfile(filepath)
	elif platform.startswith('linux') : subprocess.call(('xdg-open', filepath))

def sort_on_runtime(pos):	#optimizes emcee's MPI pool
	p = numpy.atleast_2d(pos)
	idx = numpy.argsort(p[:,0])[::-1]
	return p[idx], idx

if __name__ == '__main__':

	# Setting IncludeAlpha = True, results in the sampling of a mass dependent ionising efficiency.
	# This mass dependent ionising efficiency is defined as (Mass / M_vir)**(Alpha)
	# - M_vir is the mass corresponding to the Virial Temperature (T_Vir) evaluated at the respective redshift (T_vir is defined to be redshift independent)
	# - Alpha is a power law. Alpha = 0 corresponds to a mass independent ionising efficiency, corresponding to the default 3 parameter reionisation model
	# IncludeAlpha = True allows for a 4 parameter model to be sampled. If Includealpha is False, then we only consider the three parameter model (constant ionising 	 # efficiency) for the streamlined 21cmFAST semi-numerical simulation.
	# To sample the parameter space better, we deal with the log10 of the virial temperature.

	params = numpy.array(params)
	Redshift = numpy.array(redshift)  
	
	# 21CMMC picked Redshifts Here --> became a global variable within Cosmohammer but instead its global here so
	 #that the data can be saved from the Likelihood.
	

	############################################################## - defining functions after specifying dimensions and params

	def lhood(p, ndim, nparams):
		
		z = numpy.zeros(len(Redshift))
		t = chain(p) 
		# the format of is: chain(p) = ((Log_e{L}, array([ Nf(z=redshift[0]),  ...,  Nf[redshift[N]])), {}) 
		# this is the likelihood and Neutral fraction (Nf at each redshift) value respectively. 

		t, z = t[0]  
		
		print "Ln Likelihood: ", t

		#returns the Likelihood only
		return t	

	def emceelikelihood_hardedges(p):
		L = 0.
		for i in range(ndim):
			if(params[i][1]>p[i] or params[i][2]<p[i]): 
				L = -1.*numpy.inf
				print 'A walker tried to go out of bounds!'
		if (L == 0.): return lhood(p, ndim, ndim)
		return L

	def emceelikelihood_softedges(p):   
		#when a walker tries to go out of bounds the observational data in Parameter_Files is called... 
		#if there is no data where the params are called from the run will freeze (waiting for non-existent data) 
		#(I prefer to use the hard edges because of this).
		return lhood(p, ndim, ndim)	


	#used prior is uniform-->multinest requests the transform of a [0,1] unit cube prior 

	def prior(p, ndim, nparams): 	
		for i in range(ndim):
			p[i] = (params[i][1] + p[i]*(params[i][2]-params[i][1]))

	#alternate prior if necessary: Log (with hard edges) #Will Error if alpha prior contains negatives
	
	def logPrior(p, ndim, nparams): 	
		for i in range(ndim):
			p[i] = 10.**(numpy.log10(params[i][1]) + p[i]*(numpy.log10(params[i][2])-numpy.log10(params[i][1])))

	#alternate prior if necessary: gaussian (with hard edges) 

	def GaussianPrior(p):#, ndim, nparams): 
		
		nparams = ndim 
		x = numpy.zeros(shape=(ndim))
		t = numpy.zeros(shape=(ndim), dtype=bool)
	
		test = False	
		while(test!=True):
		
			for i in range(ndim):
				x[i] = numpy.random.normal(loc=params[i][0], scale=params[i][3], size=None)
		
			for i  in range(ndim):
				if(x[i] < params[i][2] and x[i] > params[i][1]): 
					t[i] = True 
				else: 
					t[i] = False
				
			s = numpy.all(t)
			if(s == True): 
				test = True
		
		for i in range(ndim):
			p[i] = x[i]


	######################################################################## - functions stop here

	#when more than 2 data are compared against I intend this to be where we can switch between them:
	
	if (Fiducial_set==1.):	#fiducial set 1 (f1) - produced by the 3p model	
	
		Zeta_val = 20.0  
		MFP_val = 15.0
		TVir_val = 30000.0
		Alpha_val = 0.0			#(hence zeta=zeta_0)


	if (Fiducial_set==2.):	#fiducial set 2 (f2)  - produced by the 4p model )

		Zeta_val = 15.0  #Zeta_0 
		MFP_val = 15.0
		TVir_val = 50000.0
		Alpha_val = 0.4	 #(\Zeta = \Zeta_0*(Tvir/Tvirfeed)**\alpha

	if (Fiducial_set==3.):	#fiducial set 2 (f2)  - produced by the 4p model )

		Zeta_val = 30.0  
		#MFP_val = 15.0
		TVir_val = 4.5 #this is just a label.. 
		#Alpha_val = 0.4

	# Reionisation redshifts for the multi-z 21cm Fast "observations"
	# Need to make sure that these boxes exist in the "Boxes" folder. If not, please generate new boxes
	# Shifted the range to lower-z, motivated by the lower Planck (2016) values of tau
	# Expanded the default range to allow for an approximate computation of tau, to be applied as a prior
	
	

	# Added some basic support for observational priors on the IGM neutral fraction, or reionisation history from Planck tau
	# (can use the form of these priors to include your own priors on individual parameters, or for global quantites)
	# Provide three possible observational priors:
	# 1) Constraints on tau from Planck (2016) (https://arxiv.org/abs/1605.03507)
	# 2) Limit on the IGM neutral fraction at z = 5.9, from dark pixels by I. McGreer et al. (2015) (http://adsabs.harvard.edu/abs/2015MNRAS.447..499M)
	# 3) Constraints on the IGM neutral fraction at z = 7.1 from the IGM damping wing of ULASJ1120+0641 Greig et al (2016) (http://arxiv.org/abs/1606.00441)
	
	# NOTE: Constraints are set in 21CMN/likelihood/Likelihood21cmFast.py
	# Go here to change any of the prior behaviour, or to add additional priors not provided below.

	# NOTE: If any priors are to be added, it is recommended that at least three redshifts are considered. 
	# PROVIDING LESS THAN THREE REDSHIFTS WILL CAUSE THE USE OF PRIORS TO BE TURNED OFF (i.e PRIORS WILL NOT BE INCLUDED FOR LESS THAN THREE REDSHIFTS)
	# These priors require interpolation/extrapolation of the reionisation history, therefore, to aid accuracy at least three should be selected.

	# NOTE: If the redshifts selected by the user ** is ** at the redshift of the prior, then interpolation is not required, and the prior will be used!
	
	# 0) 21cmNest adds capability to remove the 21cm PS from the Likelihood calculation to assess the improvements in Bayes factor 
	Include21cm = True
	# 1) The Planck prior is modelled as a Gaussian: tau = 0.058 \pm 0.012
	IncludePlanck = True
	# 2) The McGreer et al. prior is a upper limit on the IGM neutral fraction at 5.9
	# Modelled as a flat, unity prior at x_HI <= 0.06, and a one sided Gaussian at x_HI > 0.06 ( Gaussian of mean 0.06 and one sigma of 0.05 )
	IncludeMcGreer = True
	# 3) The Greig et al. prior is computed directly from the PDF of the IGM neutral fraction for the Small HII reionisation simulation
	IncludeGreig = True
	
	PriorLegend = dict()
	PriorLegend['21cmPS'] = Include21cm
	PriorLegend['PlanckPrior'] = IncludePlanck
	PriorLegend['McGreerPrior'] = IncludeMcGreer
	PriorLegend['GreigPrior'] = IncludeGreig

	# If the QSO damping wing constraint is set, need to read in the PDF data from the provided text file.
	NFVals_QSODamping = []
	PDFVals_QSODamping = []

	if IncludeGreig is True:

		with open('inputData/PriorData/NeutralFractionsForPDF.out','rb') as handle:
			NFVals_QSODamping =	pickle.loads(handle.read())

		with open('inputData/PriorData/NeutralFractionPDF_SmallHII.out','rb') as handle:
			PDFVals_QSODamping = pickle.loads(handle.read())

		# Normalising the PDF to have a peak probability of unity (consistent with how other priors are treated)
		# Ultimately, this step does not matter
		normalisation = numpy.amax(PDFVals_QSODamping)

		PDFVals_QSODamping = PDFVals_QSODamping/normalisation

	multi_z_mockobs_k = []
	multi_z_mockobs_PS = []

	separator = "_"
	seq = []
	seq.append("%s"%(Zeta_val))
	if (Fiducial_set == 1.0 or Fiducial_set == 2.0): 	
		seq.append("%s"%(MFP_val))
	seq.append("%s"%(TVir_val))
	if (Fiducial_set == 1.0 or Fiducial_set == 2.0): 
		seq.append("%s"%(Alpha_val))

	StringArgument = string.join(seq,separator)

	# Read in the mock 21cm PS observation. Read in both k and the dimensionless PS. These are needed for performing the chi^2 statistic for the likelihood
	# Note: To calculate the likelihood statistic a spline is performed for each of the mock PS, simulated PS and the Error PS

	for i in range(len(Redshift)):		
		mockobs_k_values = numpy.loadtxt('inputData/NoiseData/MockObs_PS_250Mpc_z%s_%s.txt'%(Redshift[i],StringArgument), usecols=(0,))
		mockobs_PS_values = numpy.loadtxt('inputData/NoiseData/MockObs_PS_250Mpc_z%s_%s.txt'%(Redshift[i],StringArgument), usecols=(1,))

		multi_z_mockobs_k.append(mockobs_k_values)
		multi_z_mockobs_PS.append(mockobs_PS_values)

	multi_z_mockobs_k = numpy.array(multi_z_mockobs_k)
	multi_z_mockobs_PS = numpy.array(multi_z_mockobs_PS)	

	# Set for the desired telescope ['SKA_halveddipoles_compact', 'HERA331'] corresponding to the file structure in "NoiseData"
	multi_z_Error_k = []
	multi_z_Error_PS = []

	# Total noise sensitivity as computed from 21cmSense. This total noise combines both the Poisson component of the observationally measured PS 
	# (the high res 21cm FAST simulation) and the instrument thermal noise profile.
	# Note: These will be interpolated in the computation of the likelihood statistic

	#TotalError_%s_PS_250Mpc_z%s_%s_1000hr.txt'%(Telescope_Name,Redshift[i],StringArgument)

	Telescope_Name = Telescope
	Obs_time = Observing_time	

	for i in range(len(Redshift)):
		Error_k_values = numpy.loadtxt('inputData/NoiseData/TotalError_%s_PS_250Mpc_z%s_%s_%s.txt'%(Telescope_Name,Redshift[i],StringArgument,Obs_time), usecols=[0])	
		Error_PS_values = numpy.loadtxt('inputData/NoiseData/TotalError_%s_PS_250Mpc_z%s_%s_%s.txt'%(Telescope_Name,Redshift[i],StringArgument,Obs_time), usecols=[1])

		multi_z_Error_k.append(Error_k_values)
		multi_z_Error_PS.append(Error_PS_values)

	multi_z_Error_k = numpy.array(multi_z_Error_k)
	multi_z_Error_PS = numpy.array(multi_z_Error_PS)

	
	
	
	# k-space cut to be made to the likelihood fitting corresponding to the removal of foregrounds
	foreground_cut = 0.15
	shot_noise_cut = 1.0

	# Including a modelling uncertainty. This accounts for differences between semi-numerical and N-body/Hydro/RT prescriptions for EoR simulations. Setting to 0.0
	# will remove the uncertainty. 0.10 corresponds to 10 per cent modelling uncertainty
	ModUncert = 0.10
	
	# Bad value flag returns a value far away from the highest likelihood value to ensure the MCMC never enters into this region. This flag will be typically 
	# raised for a measured PS from a completely ionised simulation box. Hence, is just the chi^{2} for the PS itself (i.e. PS zero) multiplied by 100 to really
	# ensure this parameter combination can never be chosen
	bad_value_flag = -0.5*(100.*sum(numpy.square((multi_z_mockobs_PS[0])/multi_z_Error_PS[0])))
	
	if len(Redshift) == 1:
		multiz_flag = 'singlez'
	else:
		multiz_flag = 'multiz'

# Parameter range for ionizing efficiency, mean free path, log_10(virial temperature) and alpha respectively.	
# (to sample the parameter space better, we deal with the log10 of the virial temperature)
# If Incl alphaAlpha_val is zero, then only considering the three parameter model (constant ionising efficiency)		
# Parameter range for the power law alpha, for a mass dependendant ionising efficiency. Alpha == 0, corresponds to mass independent ionising efficiency
	
	chain = LikelihoodComputationChain()  
	
	num_redshifts = len(Redshift)
			
	if num_redshifts < 3:

		if PriorLegend['PlanckPrior'] is True:
			print 'The planck prior will not be used as insufficient redshift boxes have been chosen'
			print 'A minimum of three independent redshifts are required to estimate tau'

		if PriorLegend['McGreerPrior'] is True:
			if McGreer_Redshift not in Redshift:
				print 'The McGreer et al. prior will not be used as insufficient redshift boxes have been chosen'
				print 'A minimum of three independent redshifts are required to interpolate/extrapolate the reionisation history'				

		if PriorLegend['GreigPrior'] is True:		
			if QSO_Redshift not in Redshift:
				print 'The Greig et al. QSO Damping Wing prior will not be used as insufficient redshift boxes have been chosen'
				print 'A minimum of three independent redshifts are required to interpolate/extrapolate the reionisation history'				
	NF_filecheck = os.path.isfile('outputData/NF_' + Model_flag + '.txt')	

	if NF_filecheck:

		comd = 'rm outputData/NF_' + Model_flag + '.txt'
		os.system(comd) 

	Likelihoodmodel21cmFast = Likelihood21cmFast_multiz(multi_z_mockobs_k,multi_z_mockobs_PS,multi_z_Error_k,multi_z_Error_PS,
		Redshift, ndim, foreground_cut,shot_noise_cut,ModUncert,PriorLegend,NFVals_QSODamping,PDFVals_QSODamping, Model_flag, ParamsZdependency, NFflag_for1pZdepMHR)  #params? - sampler does them.
	
	chain.addLikelihoodModule(Likelihoodmodel21cmFast) # name.append, adds the Likelihood Module 'Likelihoodmodel21cmFast' to the chain instance of the Likelihood21cmFast_mulitz class.

	chain.setup()	#calls the instance  of the class Likelihood21cmFast_multiz, the method setup() prints "Likelihood Fitting for 21cm Fast" from this class, if a that instance has been given an adequate Likelihood module (e.g. Likelihood21cmFast_multiz contains the attribute setup() ) ALPHA

	#becareful choosing the parameters (wrt n_dim and n_params), [Zeta=ionizing efficiency,Rmfp, log10(Tvir)], aren't the only ones: # there is Nf_val and possibly [alpha= adding a mass dependancy to the ionising efficiency]. e.g. Nf_val is not independent of redshift, which is not a parameter here.
	
	if (Fiducial_set == 1.0 or Fiducial_set == 2.0): 	
		print '\n Beginning to run the with the', sampler,' sampler with the', ndim, 'parameter ', Model_flag , ' model as seen by', Telescope,' with ', Obs_time, ' of Observing time.\n Fitting for 21cmFAST(FZH) with fiducial values: ', Zeta_val, MFP_val, TVir_val, Alpha_val, 'at/interpolated between redshift(s):', Redshift	
	
	if (Fiducial_set == 3.0): 	
		print '\n Beginning to run the with the', sampler,' sampler with the', ndim, 'parameter ', Model_flag , ' model as seen by', Telescope,' with ', Obs_time, ' of Observing time.\n Fitting for MHR with fiducial values: ', Zeta_val, TVir_val, 'at/interpolated between redshift(s):', Redshift

#pymultinest factory settings: https://johannesbuchner.github.io/PyMultiNest/pymultinest_run.html 
 	#pymultinest.run(callable, prior, n_dims=3, n_params=3, n_clustering_params=None, wrapped_params=None, importance_nested_sampling=True, multimodal=True, const_efficiency_mode=False, n_live_points=400, evidence_tolerance=0.5, sampling_efficiency=0.8, n_iter_before_update=100, null_log_evidence=-1e+90, max_modes=100, mode_tolerance=-1e+90, outputfiles_basename="%s"%(File_String), seed=-1, verbose=False, resume=False, context=0, write_output=True, log_zero=-1e+100, max_iter=10000, init_MPI=False, dump_callback=None) 	

# Running 21CMNest:  simply $ python 21CMNest.py
#[if you want the evidence/points data, it can be done using $ python 21CMNest.py |& tee -a EvidenceProgress.txt 
# this puts the evidence per livepoint update value from the terminal in a text file.]
#for MPI leave MPI_init=False, and run with $ mpirun -np #cores python 21CMNest.py

#typically n_live_points=2000, (see livepoint figure (fig2 i think) in the 21CMNest paper.)

#	max_iter=0, (unlimited iterations)	
#	sampling efficiency = 0.3 (API says this is the best for Model Selection, 0.8 is best for parameter estimation)
# 	Using resume=True will read in from file the last locations of the iso-likelihood contours to "continue" the sampling if the sampler hasn't sufficently converged.
	print params

	if (sampler == 'Multinest'):
		
		
		File_String = 'ReionModel_21CMNest_%s_%s'%(Telescope_Name,multiz_flag)

		pymultinest.run(lhood,	
			prior,	
			n_dims = ndim,		
			n_params = ndim,		 
			n_live_points = 1,
			resume=False,
			verbose=True,
			write_output=True,
			outputfiles_basename="outputData/%s"%(File_String),
			max_iter=1,
			importance_nested_sampling=True,
			multimodal=True,
			evidence_tolerance=0.5,
			sampling_efficiency=0.3
			)
		
	#	attached emcee sampler for comparisons...   it ONLY runs in mpi ($ mpirun -np 16 python 21CMNest.py) for 16 cores
	
	if (sampler == 'Emcee'):

		File_String = 'ReionModel_21CMMC_%s_%s'%(Telescope_Name,multiz_flag)

		nwalkers = 32
		
		pool = MPIPool(loadbalance=True)   
		if not pool.is_master():
			pool.wait()
			sys.exit(0)

		p0 = numpy.random.rand(ndim*nwalkers).reshape((nwalkers, ndim))

		for i in range(ndim):
			for j in range(nwalkers):
				p0[j][i] = params[i][1] + (params[i][2]-params[i][1])*p0[j][i]

		sampler = emcee.EnsembleSampler(nwalkers, ndim, emceelikelihood_hardedges, pool=pool, runtime_sortingfn=sort_on_runtime, live_dangerously=False, a=2.) 
		burnin = 50 
		points = 10000  # NOTE: it is this many points PER WALKER!
		
		print '\n Starting Burn in:'			
		
		pos, prob, state = sampler.run_mcmc(p0, burnin)

		chain = numpy.asarray(sampler.flatchain)		
		data = numpy.zeros((len(chain), ndim+2))
		
		data[:,0] = 1. #equally weighted points (now the data is in the same format as cosmoMC)
		data[:,1] = sampler.flatlnprobability
		data[:,2:ndim+2] = sampler.flatchain 

		numpy.savetxt('outputData/Burn_in.txt', data)

 
		sampler.reset()

		print '\n Burn in complete. Running the sampler: \n'	

		x = sampler.run_mcmc(pos,points)	

		pool.close()

		chain = numpy.asarray(sampler.flatchain)		
		data = numpy.zeros((len(chain), ndim+2))
		
		data[:,0] = 1. #equally weighted points (now the data is in the same format as cosmoMC)
		data[:,1] = sampler.flatlnprobability
		data[:,2:ndim+2] = sampler.flatchain 

		numpy.savetxt('outputData/%s.txt'%(File_String), data)
		
	print '\n Done!'

		      
